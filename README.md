Assignment #2:
ISHRAF UDDIN 100520689
NOUR HALABI 100525020
______________________________________________________________________________
Running program Steps:
To download you will need to clone the repository in a folder of your choice: 
git clone https://100520689@bitbucket.org/Nour_Halabi/csci2020u_assignment2.git

There are two projects folders included in the repository, one for client and another for server. Both projects should be opened separately in their own InteliJ window. 
It is important to note that the Server project should be run before the Client. Runt>main in server, once you see "Simple Http Server v1.0: Ready to handle incoming requests." you may run the Client project's main.

what will show up after running the Client's main, a set of tables will show up showing what is the local folder and the shared folder on the server side. Each project has there own folders where files are uploaded and downloaded. You may select a file from the local Folder table and press upload. You should be able to see the file has been added to the on shared folders table as well as the file has been to the correct folder.

Due to repository issues we were forced to create a new repository but the history can be viewed for this repository from this link: