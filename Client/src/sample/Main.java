package sample;

import com.sun.corba.se.spi.activation.Server;
import javafx.application.Application;
import javafx.beans.binding.StringBinding;
import javafx.beans.value.ObservableListValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Main extends Application {
    private Stage window;
    private BorderPane layout;
    private TableView<Files> ClientTable;
    private TableView<Files> ServerTable;
    private TextField sidField, assField, midField,finalExField,finalMarkFieldm, gradeField;
    private String server_download_file;
    private Files client_upload_file;
    Client clientt = new Client();

    FileHandler filehandle = new FileHandler();
    ObservableList<Files> server_list = null;




    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(root, 300, 275));
        primaryStage.show();

        server_download_file = "";
        client_upload_file = null;


        filehandle.getDirectoryList(new File("local"));


        //_____CLIENT TABLE_____//
        ClientTable = new TableView<>();
        ClientTable.setItems(filehandle.getAllLocalFiles());
        ClientTable.setEditable(true);


        //clientt.getDirectorylist();
        //clientt.getDirectory();
        //server_list = filehandle.getAllSharedFiles(clientt);


        //create table columns
        TableColumn<Files, String> client_column = null;
        client_column = new TableColumn<>("Local Files");
        client_column.setMinWidth(250);
        client_column.setCellValueFactory((new PropertyValueFactory<>("filename")));

        ClientTable.getColumns().addAll(client_column);

        //Add change listener to get the selected item from the client table.
        ClientTable.getSelectionModel().selectedItemProperty().addListener((observableValue, oldValue, newValue) -> {
            //Check whether item is selected and set value of selected item to Label
            if (ClientTable.getSelectionModel().getSelectedItem() != null)
            {
                String filename = ClientTable.getSelectionModel().getSelectedItem().getFilename();
                String text = ClientTable.getSelectionModel().getSelectedItem().getText();
                client_upload_file = new Files(filename, text);
               System.out.println(client_upload_file.getFilename()+ "\n"+client_upload_file.getText());

            }
        });


        //_____SERVER TABLE_____//
        ServerTable = new TableView<>();
        server_list = filehandle.getAllSharedFiles(clientt);
        ServerTable.setItems(server_list);
        ServerTable.getItems().addAll(server_list);
        ServerTable.setEditable(true);

        //create table columns
        TableColumn<Files, String> server_column = null;
        server_column = new TableColumn<>("Shared Files");
        server_column.setMinWidth(250);
        server_column.setCellValueFactory((new PropertyValueFactory<>("filename")));

        ServerTable.getColumns().addAll(server_column);

        //Add change listener to get the selected item from the server table.
        ServerTable.getSelectionModel().selectedItemProperty().addListener((observableValue, oldValue, newValue) -> {
            //Check whether item is selected and set value of selected item to Label
            if (ServerTable.getSelectionModel().getSelectedItem() != null)
            {
                server_download_file = ServerTable.getSelectionModel().getSelectedItem().getFilename();

            }
        });


        //_____BUTTONS_____//
        Button download_btn = new Button ("Download");
        Button upload_btn = new Button ("Upload");

        //Button for getting the directory of the shared folder needs to be updated after uploading
        Button dir_btn = new Button ("Get directory");
        Button update_client = new Button ("Update");

        download_btn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Client client = new Client();
                if(client_upload_file != null){
                    clientt.Download(server_download_file);
                }
                else {
                    System.out.println("Please select a file from the server list that" +
                            " you want to upload before selecting the button.");
                }

            }
        });

        upload_btn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                if(client_upload_file != null){

                  //  ServerTable.getItems().addAll(server_list);
                     clientt.Upload(client_upload_file);
                    ServerTable.getItems().addAll(filehandle.getAllSharedFiles(clientt));
                   // ServerTable.getItems().add(client_upload_file);
                }
                else {
                    System.out.println("Please select a file from the client list that" +
                            " you want to upload before selecting the button.");
                }

            }
        });




        //ServerTable.getColumns().addAll(server_column);

        GridPane editarea = new GridPane();
        editarea.add(download_btn,0,0);
        editarea.add(upload_btn,1,0);

        layout = new BorderPane();
        layout.setTop(editarea);
        layout.setLeft(ClientTable);
        layout.setRight(ServerTable);

        Scene scene = new Scene(layout, 500, 600);
        primaryStage.setScene(scene);

        primaryStage.show();

    }


    public static void main(String[] args) {
        launch(args);


    }
}