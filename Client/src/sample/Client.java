package sample;

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.StringTokenizer;

public class Client {
    ArrayList<String> directorylist;

    public ArrayList<String> getDirectorylist() {
        return this.directorylist;
    }

    public void setDirectorylist(ArrayList<String> directorylist) {
        this.directorylist = directorylist;
    }

    public Client () {
        //Socket
        this.directorylist = new ArrayList<>();
    }

    //Issue command to begin downloading by
    //asking the server to send the file contents
    public void Download(String filename)
    {
        try
        {
            String tag = "DOWNLOAD";

            Socket soc = new Socket("localhost",8080);

            //Send the command
            PrintWriter out = new PrintWriter(soc.getOutputStream());
            out.print(tag +" "+ filename); //Format: DOWNLOAD filename
            out.flush();

            //Read in response
            BufferedReader in = new BufferedReader(new InputStreamReader(soc.getInputStream()));

            String response;
            while ((response = in.readLine()) != null) {
                System.out.println(response);
                FileHandler filehandler = new FileHandler();
                filehandler.SaveTextToFile(response, new File("local/"+filename));
            }

            // close the connection (3-way tear down handshake)
            out.close();
            in.close();
            soc.close();

        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }


    //Issue command to upload file to the server by send the contents over to server
    public void Upload(Files file)
    {
        try
        {
            String tag = "UPLOAD";
            String temp = "";

            Socket soc = new Socket("localhost",8080);

            //Send the command
            PrintWriter out = new PrintWriter(soc.getOutputStream());
            out.print(tag +" "+ file.getFilename() + " " + file.getText()); //Format: UPLOAD filename file-content
            out.flush();

            //Read in response
            BufferedReader in = new BufferedReader(new InputStreamReader(soc.getInputStream()));

            //Get directory list to update the client side
            String response;
            while ((response = in.readLine()) != null) {
                System.out.println("respons: " + response);
                temp += response;
                String[] result = response.split(",");
                String text = "";
                for(int i = 0; i < result.length ; i++) {

                    directorylist.add(result[i]);
                    System.out.println(result[i]);

                    text += result[i];
                    text += ",";
                    FileHandler fh = new FileHandler();
                    fh.SaveTextToFile(text, new File("server_list"));
                }
            }
            System.out.print("result: " + temp);

            // close the connection (3-way tear down handshake)
            out.close();
            in.close();
            soc.close();

        }
        catch(IOException e)
        {
            e.printStackTrace();
        }

    }

    //Issue command to retrieve shared folder directory list
    public void getDirectory()
    {
        try
        {
            String tag = "DIR";

            Socket soc = new Socket("localhost",8080);

            //Send the command
            PrintWriter out = new PrintWriter(soc.getOutputStream());
            out.print(tag); //Format: DIR
            out.flush();

            //Read in response
            BufferedReader in = new BufferedReader(new InputStreamReader(soc.getInputStream()));

            //Get directory list to update the client side
            String response;
            while ((response = in.readLine()) != null) {
                System.out.println(response);
                String[] result = response.split(",");
                for(int i = 0; i < result.length ; i++) {
                    System.out.println(result[i]);
                    directorylist.add(result[i]);
                }

            }

            // close the connection (3-way tear down handshake)
            out.close();
            in.close();
            soc.close();

        }
        catch(IOException e)
        {
            e.printStackTrace();
        }

    }


//
//    public static void main(String[] args) {
//        Socket socket;
//        BufferedReader in;
//        PrintWriter out;
//
//        if (args.length == 0) {
//            System.out.println("Usage:  java HttpClient <host> [<port>=80]");
//            System.exit(0);
//        }
//
//        // get the host name
//        String hostName = args[0];
//
//        // get the port number, if there is one
//        int portNumber = 80;
//        if (args.length > 1) {
//            portNumber = Integer.parseInt(args[1]);
//        }
//
//        try {
//            // connect to the server (3-way connection establishment handshake)
//            socket = new Socket(hostName, portNumber);
//
//            // wrap the input streams into readers and writers
//            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
//            out = new PrintWriter(socket.getOutputStream());
//
//            // send the request
//            String request = "GET /yahoo/yahoo.html HTTP/1.0";
//            String delim = "\r\n";
//            out.print(request + delim + delim);
//            out.flush();
//
//            // read and print the response
//            String response;
//            System.out.println("Response:");
//            while ((response = in.readLine()) != null) {
//                String[] result = response.split(",");
//                for(int i = 0; i < result.length ; i++) {
//                    System.out.println(result[i]);
//                }
//
//
//            }
//
//            // close the connection (3-way tear down handshake)
//            out.close();
//            in.close();
//            socket.close();
//        } catch (IOException e) {
//            System.out.println(e);
//            e.printStackTrace();
//        }
//    }


}
